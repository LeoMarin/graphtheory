#pragma once

#include <vector>

// Test Case Declaration
enum class TSPTestCase
{
	TestCase1
};

// TeavellingSalesman Class Declaration
template <int size>
class TravellingSalesman
{
public:
	// TravellingSalesman Public Methods
	TravellingSalesman();
	// Test case with size 4
	TravellingSalesman(TSPTestCase testCase);
	~TravellingSalesman() {}
	
	void PrintAdjecencyMatrix();

	// Getter function used in testing
	const int* GetAdjecencyMatrix() { return (const int*)adjacencyMatrix; }

	// Find the cost of the shortest path
	int ShortesPathCost(int startingNode);

	// Helper functions

	// Returns true if Ith bit in subset is 0
	bool NotIn(int i, int subset);
	// Generates all posible bit sets with "size" bits and r bits set to 1
	std::vector<int> Combinations(int r);

private:
	// TravellingSalesman Private Methods

	// Helper function for generating bit sets
	void Combinations(int set, int at, int r, std::vector<int>& subsets);
private:
	// TravellingSalesman Private Data

	// Adjecency Matrix containing all edge weights
	int adjacencyMatrix[size][size];
};

template<int size>
inline TravellingSalesman<size>::TravellingSalesman()
{
	// Loop over every cell in adjecenyMatrix and asign it a value
	// Weight from node to itself is 0
	// This is a comlete graph
	for(int i = 0; i < size; i++)
	{
		for(int j = 0; j < size; j++)
		{
			// Weights are in range 1-10 and 0 for self cycle
			adjacencyMatrix[i][j] = (i == j) ? 0 : rand() % 10 + 1;
		}
	}
}

template<int size>
inline TravellingSalesman<size>::TravellingSalesman(TSPTestCase testCase)
{
	// Size must be 4 for this test case
	adjacencyMatrix[0][0] = 0; adjacencyMatrix[0][1] = 4; adjacencyMatrix[0][2] =  1; adjacencyMatrix[0][3] = 9;
	adjacencyMatrix[1][0] = 3; adjacencyMatrix[1][1] = 0; adjacencyMatrix[1][2] =  6; adjacencyMatrix[1][3] = 11;
	adjacencyMatrix[2][0] = 4; adjacencyMatrix[2][1] = 1; adjacencyMatrix[2][2] =  0; adjacencyMatrix[2][3] = 2;
	adjacencyMatrix[3][0] = 6; adjacencyMatrix[3][1] = 5; adjacencyMatrix[3][2] = -4; adjacencyMatrix[3][3] = 0;
	// Expected minimum cost is 9
}

template<int size>
inline void TravellingSalesman<size>::PrintAdjecencyMatrix()
{
	for(int i = 0; i < size; i++)
	{
		for(int j = 0; j<size;j++)
			std::cout << adjacencyMatrix[i][j] << "\t ";
		std::cout << std::endl;
	}
}

template<int size>
inline int TravellingSalesman<size>::ShortesPathCost(int startingNode)
{
	// Initialize Memo table used for story current dynamic state
	// Fill it with +infinity values
	// memo = size * (2^size)
	// rows represent last visited node
	// columns represent all visited nodes in current path
	// example = column 3 = 0011 (binary) -> 0th and 1st node visited
	int const column = pow(2, size); 
	
	std::vector<std::vector<int>> memo(size, std::vector<int>(column));
	for(int i = 0; i < size; i++)
	{
		for(int j = 0; j < size; j++)
			memo[i][j] = std::numeric_limits<int>::max();
	}

	// Setup memo table by storing optimal solution from the start node
	// to the every other node in the graph

	// Loop over every node
	for(int i = 0; i < size; i++)
	{
		// Skip starting node
		if(i == startingNode) continue;

		// Store optimal values from starting node to all other nodes
		memo[i][1 << startingNode | 1 << i] = adjacencyMatrix[startingNode][i];
	}

	// Solve TSP by filling up memo table

	// Loop over every subpath, starting from 3
	// r represents number of visited nodes in current subpath
	for(int r = 3; r <= size; r++)
	{
		// Generate all subsets with r active bits
		std::vector<int> subsets = Combinations(r);
		for(auto& subset : subsets)
		{
			// Skip subsets that dont have starting node active
			if(NotIn(startingNode, subset)) continue;

			// Loop over every node, iterating next node
			for(int next = 0; next < size; next++)
			{
				// Skip nodes that are not in current subset
				if(next == startingNode || NotIn(next, subset)) continue;
				
				// Subset excluding current node
				int state = subset ^ (1 << next);
				int minDistance = std::numeric_limits<int>::max();

				// Loop over all nodes iterating over end node
				for(int endNode = 0; endNode < size; endNode++)
				{
					// Skip if endNode is equal to startin or next and if it is not in subset
					if(endNode == startingNode || endNode == next || NotIn(endNode, subset)) continue;

					// Calculate new distance and save it if it is better than previus minimum
					int newDistance = memo[endNode][state] + adjacencyMatrix[endNode][next];
					if(newDistance < minDistance) 
						minDistance = newDistance;
				}
				// Save minimum distance to memo table
				memo[next][subset] = minDistance;
			}
		}
	}

	// Calculate minimm distance

	// Index of paths that have visited all other nodes
	int endState = pow(2, size) - 1; 
	int minCost = std::numeric_limits<int>::max();

	for(int i = 0; i < size; i++)
	{
		if(i == startingNode) continue;

		// cost to visit all nodes + cost from last node to first
		int cost = memo[i][endState] + adjacencyMatrix[i][startingNode];
		
		if(cost < minCost)
			minCost = cost;
	}

	return minCost;
}

template<int size>
inline bool TravellingSalesman<size>::NotIn(int i, int subset)
{
	return ((1 << i) & subset) == 0;
}

template<int size>
inline std::vector<int> TravellingSalesman<size>::Combinations(int r)
{
	std::vector<int> subsets;
	Combinations(0, 0, r, subsets);
	return subsets;
}

template<int size>
void TravellingSalesman<size>::Combinations(int set, int at, int r, std::vector<int>& subsets)
{
	if(r == 0)
		subsets.push_back(set);
	else
	{
		for(int i = at; i < size; i++)
		{
			// Flip on Ith bit
			set = set | (1 << i);

			Combinations(set, i + 1, r - 1, subsets);

			// Backtrack and flip off Ith bit
			set = set & ~(1 << i);
		}
	}
}
