#pragma once

#include <vector>

// Edge Type Declaration
enum class EdgeType
{
	Unweighted, Weighted, WeightedNegative
};

// Test Case Declaration
enum class TestCase
{
	NoShortestPath, BFSShortestPath, Dijkstra, NegativeCycle, NoNegativeCycle
};

// Edge Declaration
struct Edge
{
	Edge(int vertexIndex, int weight) : vertexIndex(vertexIndex), weight(weight) {}

	// Index of vertex that edge is pointing at
	int vertexIndex;
	int weight;
};

// Vertex Declaration
struct Vertex
{
	Vertex(int value) : value(value) {}

	// Dummy data vertex is holding
	int value;
	// List of outgoing edges
	std::vector<Edge> edges;

	// Used in most traversal algorithms
	bool visited = false;
};

// Graph Declaration
class Graph
{
public:
	// Graph Public Methods
	Graph(size_t numOfVertices, int minEdges, int maxEdges, EdgeType edgeType, bool directed, bool acyclic);
	Graph(TestCase testCase);
	~Graph();

	// Print all edges and vertices
	void PrintGraph();
	// Reset Vertex.visited to false for all vertices
	void ResetGraph();

	// Getter function used in testing
	const std::vector<Vertex>& GetVertices() const { return vertices; }

	// Traversal Algorithms
	 
	// Bredth First Searh
	void BFSTraversal();
	// Depth First Search
	void DFSTraversal();

	// Shortest Path algorithms
	
	// Bredth First Searh
	std::vector<int> BFSShortestPath(int startVertex, int endVertex);
	// Dijkstras algoritam for shortest path
	std::vector<int> Dijkstra(int startVertex, int endVertex);

	// Detect Negative Cycles algorithms
	
	// Bellman Ford algorithm for detecting negatice cycles
	bool BellmanFordNegativeCycle();

protected:
	// Graph Protected Data

	// List of all vertices in the graph
	std::vector<Vertex> vertices;
};