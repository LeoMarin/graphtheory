#include <iostream>
#include <queue>
#include <stack>
#include <limits>

#include "graph.h"


Graph::Graph(size_t numOfVertices, int minEdges, int maxEdges, EdgeType edgeType, bool directed, bool acyclic)
{
	// Reserve memory for list to avioid realocation
	vertices.reserve(numOfVertices);
	
	// Generate vertices with ascending values
	for(int i = 0; i < numOfVertices; i++)
	{
		vertices.push_back(Vertex(i));
	}

	// Generate Edges
	// Loop over every vertex
	for(size_t i = 0; i < numOfVertices; i++)
	{
		// Last node, no new edges
		if(!directed || acyclic)
			if(i == numOfVertices - 1)
				break;

		// Calculate number of edges for current vertex
		int numOfEdges = rand() % maxEdges + minEdges;
		numOfEdges -= vertices[i].edges.size();

		// Create every edge
		for(size_t j = 0; j < numOfEdges; j++)
		{
			// Chose one vertex
			int targetVertex = 0;
			if(!directed || acyclic)
			{					
				// If graph is not directed or acyclic
				// Get one of the remaining vertices
				targetVertex = rand() % (numOfVertices - i - 1) + i + 1;
			}
			else
			{
				// Get any vertex
				targetVertex = rand() % numOfVertices;
			}

			// Check if edge exists
			bool edgeExists = false;
			for(auto& edge : vertices[i].edges)
			{
				if(edge.vertexIndex == targetVertex)
				{
					edgeExists = true;
					break;
				}
			}
			if(edgeExists)
				break;

			// Calculate edge weight
			int weight = 0;
			switch(edgeType)
			{
			case EdgeType::Unweighted:
				weight = 0;
				break;
			case EdgeType::Weighted:
				weight = rand() % 15 + 1;
				break;
			case EdgeType::WeightedNegative:
				weight = rand() % 20 - 5;
				break;
			default:
				weight = 0;
				break;
			}

			// Add current edge
			vertices[i].edges.push_back(Edge(targetVertex, weight));
			// If graph is undirected add edge in opposite direction
			if(!directed)
				vertices[targetVertex].edges.push_back(Edge(i, weight));
		}
	}
}

Graph::Graph(TestCase testCase)
{
	// Constructor used in testing

	// Create 5 vertices
	for(int i = 0; i < 5; i++)
		vertices.push_back(Vertex(i));

	// Based on test case create edges
	switch(testCase)
	{
	case TestCase::NoShortestPath:
		// No path from first to last node
		vertices[0].edges.push_back(Edge(1, 1));
		vertices[0].edges.push_back(Edge(3, 1));
		vertices[1].edges.push_back(Edge(2, 1));
		vertices[2].edges.push_back(Edge(0, 1));
		vertices[4].edges.push_back(Edge(1, 1));
		// Expected result "No path"
		break;
	case TestCase::BFSShortestPath:
		// Two paths from first to last
		vertices[0].edges.push_back(Edge(1, 0));
		vertices[0].edges.push_back(Edge(3, 0));
		vertices[1].edges.push_back(Edge(2, 0));
		vertices[2].edges.push_back(Edge(3, 0));
		vertices[3].edges.push_back(Edge(4, 0));
		vertices[4].edges.push_back(Edge(2, 0));
		// Expected result "0, 3, 4"
		break;
	case TestCase::Dijkstra:
		// Two paths from first to last
		vertices[0].edges.push_back(Edge(1, 2));
		vertices[0].edges.push_back(Edge(4, 10));
		vertices[1].edges.push_back(Edge(2, 1));
		vertices[2].edges.push_back(Edge(4, 3));
		vertices[3].edges.push_back(Edge(4, 5));
		vertices[4].edges.push_back(Edge(2, 3));
		// Expected result "0, 1, 2, 4"
		break;
	case TestCase::NegativeCycle:
		// Two paths from first to last
		vertices[0].edges.push_back(Edge(1, 2));
		vertices[0].edges.push_back(Edge(4, 3));
		vertices[1].edges.push_back(Edge(3, -5));
		vertices[2].edges.push_back(Edge(4, 3));
		vertices[3].edges.push_back(Edge(4, 5));
		vertices[3].edges.push_back(Edge(0, 1));
		vertices[4].edges.push_back(Edge(2, 3));
		// Expected result true
		break;
	case TestCase::NoNegativeCycle:
		// Two paths from first to last
		vertices[0].edges.push_back(Edge(1, 2));
		vertices[0].edges.push_back(Edge(4, 10));
		vertices[1].edges.push_back(Edge(2, 1));
		vertices[2].edges.push_back(Edge(4, 3));
		vertices[3].edges.push_back(Edge(4, 5));
		vertices[4].edges.push_back(Edge(2, 3));
		// Expected result false
		break;
	default:
		break;
	}

}

Graph::~Graph()
{
}

void Graph::PrintGraph()
{
	// Print all vertices and their respective edges

	std::cout << "vertices : edges (target, weight)" << std::endl;

	// Loop over every vertex
	for(int i = 0; i < vertices.size(); i++)
	{
		std::cout << i << ": ";

		// Loop over every edge
		for(int j = 0; j < vertices[i].edges.size(); j++)
		{
			std::cout << "(" << vertices[i].edges[j].vertexIndex << ", " << vertices[i].edges[j].weight << ") ";
		}

		std::cout << std::endl;
	}

}

void Graph::ResetGraph()
{
	// Loop over every vertex and set visited to false
	for(auto& vertex : vertices)
	{
		vertex.visited = false;
	}
}

// Breadth First Search traversal algorithm
void Graph::BFSTraversal()
{
	std::cout << "BFS Traversal: ";

	// Loop over every vertex
	for(auto& vertex : vertices)
	{
		// If vertex is visited skip it
		if(vertex.visited)
			continue;

		// Setup queue
		std::queue<int> q;
		q.push(vertex.value);

		vertex.visited = true;

		// Loop while queue is not empty
		while(!q.empty())
		{
			// Get next vertex and print it
			int currentVertex = q.front();
			std::cout << vertices[currentVertex].value << ", ";

			// Remove current vertex from the queue
			q.pop();

			// Add all connected vertices that are not visited to queue
			for(auto& edge : vertices[currentVertex].edges)
				if(!vertices[edge.vertexIndex].visited)
				{
					q.push(edge.vertexIndex);
					// Mark added vertices as visited
					vertices[edge.vertexIndex].visited = true;
				}

		}
	}
	std::cout << std::endl;
}

void Graph::DFSTraversal()
{
	std::cout << "DFS Traversal: ";
	// Loop over every vertex
	for(auto& vertex : vertices)
	{
		// If vertex is visited skip it
		if(vertex.visited)
			continue;

		// Setup stack
		std::stack<int> s;
		s.push(vertex.value);
		
		// Loop while stack is not empty
		while(!s.empty())
		{
			// Get next vertex
			int currentVertex = s.top();

			// Remove current vertex from the stack
			s.pop();

			// If vertex is not visited print it and add connected vertices
			if(!vertices[currentVertex].visited)
			{
				std::cout << vertices[currentVertex].value << ", ";
				// Mark vertex as visited
				vertices[currentVertex].visited = true;

				// Add all connected vertices that are not visited to queue
				for(auto& edge : vertices[currentVertex].edges)
					if(!vertices[edge.vertexIndex].visited)
					{
						s.push(edge.vertexIndex);
					}
			}
		}
	}
	std::cout << std::endl;
}

std::vector<int> Graph::BFSShortestPath(int startVertex, int endVertex)
{
	// Breadth First Search shortest path
	// Does not take into account weight of the edges

	// Chek if nodes are out of bounds
	if(startVertex >= vertices.size() || endVertex >= vertices.size() ||
		startVertex < 0 || endVertex < 0)
	{
		std::cout << "No path" << std::endl;
		return std::vector<int>();
	}

	// create tree structure to recreate final path
	struct Node
	{
		Node(int i, Node* p) :vertexIndex(i), parent(p) {}

		int vertexIndex = 0;
		Node* parent = nullptr;
		// no need to keep track of node children
	};

	Node root{ startVertex, nullptr };

	// Keep track of all the nodes to delete them
	std::vector<Node*> nodes;

	// Setup queue
	std::queue<Node*> q;
	q.push(&root);
	vertices[startVertex].visited = true;

	int current = startVertex;

	// Loop until target vertex is found
	while(current != endVertex)
	{
		// Add all connected nodes to queue
		for(auto& edge : vertices[current].edges)
		{
			// If vertex vas not visited add it to queue
			if(!vertices[edge.vertexIndex].visited)
			{
				// Create node and add it to queue
				nodes.push_back(new Node(edge.vertexIndex, q.front()));
				q.push(nodes.back());
				// Mark as visited
				vertices[edge.vertexIndex].visited = true;
			}
		}

		q.pop();

		// no path was found
		if(q.empty())
		{
			std::cout << "There is no path from vertex " << startVertex << " to vertex " << endVertex << std::endl;
			return std::vector<int>();
		}

		// Set next element as current
		current = q.front()->vertexIndex;
	}

	// Keep track of the path
	std::vector<int> path;

	// traverse up the created tree to find the shortest path
	Node* temp = q.front();
	while(temp != nullptr)
	{
		path.push_back(temp->vertexIndex);
		temp = temp->parent;
	}

	// Make path go from front to end
	std::reverse(path.begin(), path.end());

	// Print path
	std::cout << "Shortest path is: ";
	for(int& p : path)
		std::cout << p << ", ";
	std::cout << std::endl;

	// Delete all nodes
	for(auto& node : nodes)
		delete node;

	ResetGraph();

	return path;
}

std::vector<int> Graph::Dijkstra(int startVertex, int endVertex)
{
	// Dijkstas algorithm for finding the shortest path in weighted graph

	// Chek if nodes are out of bounds
	if(startVertex >= vertices.size() || endVertex >= vertices.size() ||
		startVertex < 0 || endVertex < 0)
	{
		std::cout << "No path" << std::endl;
		return std::vector<int>();
	}

	// Create data structure to remember shortest path
	struct Distance
	{
		Distance(int previusVertex, int shortestDistance) : previusVertex(previusVertex), shortestDistance(shortestDistance) {}
		// Previus vertex in shortest path
		int previusVertex;
		// Accumulative distance
		int shortestDistance;
	};

	// Setup Dijkstras algorithm
	std::vector<Distance> distances;
	distances.push_back(Distance(startVertex, 0));
	for(int i = 1; i < vertices.size(); i++)
		distances.push_back(Distance(i, std::numeric_limits<int>::max()));
	
	// Keep track of number of visited vertices
	int visited = 0;

	// Loop while all vertices are not visited
	while(visited < vertices.size())
	{
		// Get vertex with shortest current distance
		int shortestDistance = std::numeric_limits<int>::max();
		int currentVertex = 0;
		for(int i = 0; i < distances.size(); i++)
		{
			// Find shortes distance on not visited vertex
			if(!vertices[i].visited && distances[i].shortestDistance < shortestDistance)
			{
				shortestDistance = distances[i].shortestDistance;
				currentVertex = i;
			} // Using binary heap is faster
		}

		// No possible vertex 
		if(shortestDistance == std::numeric_limits<int>::max())
			break;

		// Calculate distance for each neighbour from start for current vertex
		for(auto& edge : vertices[currentVertex].edges)
		{
			// If vertex is not visited calculate distance
			if(!vertices[edge.vertexIndex].visited)
			{
				int newDistance = distances[currentVertex].shortestDistance + edge.weight;
				// If distance is samller update the table
				if(newDistance < distances[edge.vertexIndex].shortestDistance)
				{
					distances[edge.vertexIndex].shortestDistance = newDistance;
					distances[edge.vertexIndex].previusVertex = currentVertex;
				}
			}
		}

		// Add current vertex to the visited list
		vertices[currentVertex].visited = true;
		visited++;
	}

	// If target vertex is not visited there is no path
	if(!vertices[endVertex].visited)
	{
		std::cout << "No path" << std::endl;
		return std::vector<int>();
	}


	// Read shortes path from the distances table
	std::vector<int> path;
	int temp = endVertex;
	path.push_back(temp);
	while(temp != startVertex)
	{
		// Get previus vertex
		temp = distances[temp].previusVertex;
		path.push_back(temp);
	}

	// Make path go from front to end
	std::reverse(path.begin(), path.end());

	// Print path
	std::cout << "Shortest path is: ";
	for(int& p : path)
		std::cout << p << ", ";
	std::cout << std::endl;

	ResetGraph();

	return path;
}

bool Graph::BellmanFordNegativeCycle()
{
	// Bellman Ford algorithm for detecting negative cycles

	// Setup distances table for Bellman Ford
	std::vector<int> distances(vertices.size(), 10000000);
	distances[0] = 0;

	// Relax every edge V-1 times
	for(int i = 0; i < vertices.size() - 1; i++)
	{
		// Loop over every edge in the graph
		for(auto& vertex : vertices)
		{
			for(auto& edge : vertex.edges)
			{
				// If new distance is smaller than recorded distance upadte it
				// Current distance + weight < target vertex distance
				if(distances[vertex.value] + edge.weight < distances[edge.vertexIndex])
					distances[edge.vertexIndex] = distances[vertex.value] + edge.weight;
			}
		}
	}

	// One final loop of the graph
	for(auto& vertex : vertices)
	{
		for(auto& edge : vertex.edges)
		{
			// If there is at least one smaller distance
			// Negative cycle in the graph exists
			if(distances[vertex.value] + edge.weight < distances[edge.vertexIndex])
			{
				std::cout << "Negative cycle exists" << std::endl;
				return true;
			}
		}
	}

	std::cout << "Negative cycle does not exists" << std::endl;
	return false;
}
