#include <iostream>
#include "graph.h"
#include "travelling_salesman.h"

// Test Graph Class
int TestGraph()
{
	size_t numberOfVertices = 5;
	int maxEdges = 3;
	int minEdges = 1;
	Graph graph(numberOfVertices, minEdges, maxEdges, EdgeType::WeightedNegative, true, false);

	graph.PrintGraph();
	graph.BellmanFordNegativeCycle();


	return 0;
}

int main()
{
	srand(time(NULL));
	TravellingSalesman<4> ts(TSPTestCase::TestCase1);

	std::cout << ts.ShortesPathCost(0);
}