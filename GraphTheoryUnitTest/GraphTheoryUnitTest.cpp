#include "pch.h"
#include "CppUnitTest.h"
#include "../GraphTheory/graph.h"
#include "../GraphTheory/graph.cpp"
#include "../GraphTheory/travelling_salesman.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace GraphTheoryUnitTest
{
	TEST_CLASS(GraphTheoryConstructorTest)
	{
	public:
		TEST_METHOD(GraphConstructor)
		{
			size_t numberOfVertices = 20;
			int maxEdges = 3;
			int minEdges = 1;
			Graph graph(numberOfVertices, minEdges, maxEdges, EdgeType::Unweighted, false, false);

			// Check if correct number of vertices is created
			Assert::AreEqual(numberOfVertices, graph.GetVertices().size());
		}

		TEST_METHOD(GraphReset)
		{
			size_t numberOfVertices = 20;
			int maxEdges = 3;
			int minEdges = 1;
			Graph graph(numberOfVertices, minEdges, maxEdges, EdgeType::Unweighted, false, false);

			graph.BFSTraversal();
			graph.ResetGraph();

			// Check if every vertex is marked as not visied
			for(auto& vertex : graph.GetVertices())
			{
				Assert::IsFalse(vertex.visited);
			}
		}
	};

	TEST_CLASS(GraphTheoryTraversalTest)
	{
	public:

		TEST_METHOD(BFSTraversal)
		{
			size_t numberOfVertices = 20;
			int maxEdges = 3;
			int minEdges = 1;
			Graph graph(numberOfVertices, minEdges, maxEdges, EdgeType::Unweighted, false, false);

			graph.BFSTraversal();

			// Check if every vertex is visied
			for(auto& vertex : graph.GetVertices())
			{
				Assert::IsTrue(vertex.visited);
			}
		}

		TEST_METHOD(DFSTraversal)
		{
			size_t numberOfVertices = 20;
			int maxEdges = 3;
			int minEdges = 1;
			Graph graph(numberOfVertices, minEdges, maxEdges, EdgeType::Unweighted, false, false);

			graph.DFSTraversal();

			// Check if every vertex is visied
			for(auto& vertex : graph.GetVertices())
			{
				Assert::IsTrue(vertex.visited);
			}
		}
	};

	TEST_CLASS(GraphTheoryShortesPathTest)
	{
	public:
		TEST_METHOD(BFSShortesPath)
		{
			// Check if shortest path is found
			Graph graph(TestCase::BFSShortestPath);
			std::vector<int> path = graph.BFSShortestPath(0, 4);
			std::vector<int> expectedPath{ 0, 3, 4 };

			Assert::AreEqual(path.size(), expectedPath.size());

			for(int i = 0; i < 3; i++)
				Assert::AreEqual(path[i], expectedPath[i]);
		}

		TEST_METHOD(BFSShortesPathNoPath)
		{
			// Check if graph with no path will return empty list
			Graph graph(TestCase::NoShortestPath);
			std::vector<int> path1 = graph.BFSShortestPath(0, 4);

			Assert::IsTrue(path1.empty());
		}

		TEST_METHOD(Dijkstra)
		{
			// Check if shortest path is found
			Graph graph(TestCase::Dijkstra);
			std::vector<int> path = graph.Dijkstra(0, 4);
			std::vector<int> expectedPath{ 0, 1, 2, 4 };

			Assert::AreEqual(path.size(), expectedPath.size());

			for(int i = 0; i < 3; i++)
				Assert::AreEqual(path[i], expectedPath[i]);
		}

		TEST_METHOD(DijkstraNoPath)
		{
			// Check if graph with no path will return empty list
			Graph graph(TestCase::NoShortestPath);
			std::vector<int> path = graph.Dijkstra(0, 4);

			Assert::IsTrue(path.empty());
		}
	};

	TEST_CLASS(GraphTheoryNegativeCycleTest)
	{
	public:
		TEST_METHOD(BellmanFordNoNegativeCycle)
		{
			// Check if there is any negative cycle
			Graph graph(TestCase::NoNegativeCycle);

			bool negativeCycleExists = graph.BellmanFordNegativeCycle();

			Assert::IsFalse(negativeCycleExists);
		}

		TEST_METHOD(BellmanFordNegativeCycle)
		{
			// Check if there is any negative cycle
			Graph graph(TestCase::NegativeCycle);

			bool negativeCycleExists = graph.BellmanFordNegativeCycle();

			Assert::IsTrue(negativeCycleExists);
		}
	};

	TEST_CLASS(TravellingSalesmanTest)
	{
	public:

		TEST_METHOD(TravellingSalesmanConstructor)
		{
			// Test TravelingSalesman Constructor
			TravellingSalesman<5> ts;
			const int* am = ts.GetAdjecencyMatrix();

			for(int i = 0; i < 5 * 5; i++)
			{
				int row = i / 5;
				int column = i % 5;

				// Check if weights are in range 1-10
				// And self weight is 0
				if(row == column)
					Assert::AreEqual(0, am[i]);
				else
				{
					Assert::IsTrue((am[i] < 11));
					Assert::IsTrue((am[i] > 0));
				}
			}
		}

		TEST_METHOD(TravellingSalesmanNotInHelper)
		{
			// Test TravelingSalesman NotIn helper function

			TravellingSalesman<3> ts;

			// Check if Ith bit is off in subset
			int i = 2;
			int subset = 11; // 1011
			Assert::IsTrue(ts.NotIn(i, subset));

			// Check if Ith bit is on in subset
			subset = 13; // 1101
			Assert::IsFalse(ts.NotIn(i, subset));
		}

		TEST_METHOD(TravellingSalesmanCombinationsHelper)
		{
			// Test TravelingSalesman Combinations helper function

			TravellingSalesman<4> ts;

			// Generate all subsets with 1 active bit
			std::vector<int> subsets = ts.Combinations(1);
			
			// Check if correct number is generated
			int size = subsets.size();
			Assert::AreEqual(4, size);

			// Check if correct sequence is generated
			int expected = 1;
			for(int i = 0; i < 4; i++)
			{
				Assert::AreEqual(expected, subsets[i]);
				// For 1 active bit only 2^i values will be created
				expected *= 2;
			}
		}

		TEST_METHOD(TravellingSalesmanSmallestCost)
		{
			// Test TravelingSalesman SmallestCost function

			TravellingSalesman<4> ts(TSPTestCase::TestCase1);

			int cost = ts.ShortesPathCost(0);

			// Expected shortest cost for this test case is 9
			Assert::AreEqual(cost, 9);

		}
	};
}
